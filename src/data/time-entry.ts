export declare interface TimeEntry
{
	readonly id: string,
	readonly workspace_id: string,
}
