import dbus, { ClientInterface, Variant } from 'dbus-next';


const INTERFACE_NAME = 'dev.kudera.FocusedWindow';
const PATH = '/dev/kudera/FocusedWindow';

export type OnTitleChanged = (title: string) => void;

export class WindowMonitor
{
	private readonly _listener;

	private _properties: ClientInterface | null = null;

	private _currentTitle = '';

	constructor(
		private readonly _onTitleChanged: OnTitleChanged,
	)
	{
		this._listener = (iface: string, changed: { [key: string]: unknown }) => {
			if (iface === INTERFACE_NAME && changed.CurrentTitle !== undefined) {
				const title = changed.CurrentTitle as Variant<string>;
				this.updateTitleFromVariant(title);
			}
		};
	}

	public async startMonitor(): Promise<void>
	{
		if (this._properties !== null) {
			return;
		}

		console.log('[FOCUSED_WINDOW] Starting monitor');

		const bus = dbus.sessionBus();
		const obj = await bus.getProxyObject(INTERFACE_NAME, PATH);
		this._properties = obj.getInterface('org.freedesktop.DBus.Properties');
		this._properties.addListener('PropertiesChanged', this._listener);
		const currentTitle = await this._properties.Get(INTERFACE_NAME, 'CurrentTitle') as Variant<string>;
		this.updateTitleFromVariant(currentTitle);
	}

	public stopMonitor(): void
	{
		if (this._properties === null) {
			return;
		}

		this._properties.removeListener('PropertiesChanged', this._listener);
		this._properties = null;
	}

	private updateTitleFromVariant(title: Variant<string>): void
	{
		if (title.value === this._currentTitle) {
			return;
		}

		this._currentTitle = title.value;
		this._onTitleChanged(this._currentTitle);
	}
}
