import dbus, {ClientInterface} from 'dbus-next';


export type OnIdleChanged = (idle: bigint) => void;

export class IdleMonitor
{
	private _interface: ClientInterface | null = null;

	private _running = false;

	constructor(
		private readonly _onIdleChanged: OnIdleChanged,
	)
	{
	}

	public async startMonitor(timeout: number): Promise<void>
	{
		this._running = true;
		await this.updateIdleTime(timeout);
	}

	public stopMonitor(): void
	{
		this._running = false;
		this._interface = null;
	}

	private async updateIdleTime(timeout: number): Promise<void>
	{
		if (!this._running) {
			return;
		}

		if (this._interface === null) {
			const bus = dbus.sessionBus();
			const obj = await bus.getProxyObject('org.gnome.Mutter.IdleMonitor', '/org/gnome/Mutter/IdleMonitor/Core');
			this._interface = obj.getInterface('org.gnome.Mutter.IdleMonitor');
		}

		const idle = await this._interface.GetIdletime() as bigint;
		this._onIdleChanged(idle);

		setTimeout(() => {
			this.updateIdleTime(timeout);
		}, timeout);
	}
}
