import { TimeEntry } from '../data/time-entry';

export type OnTrackingChanged = (running: boolean) => void;

export class Client
{
	private _currentEntry: TimeEntry | null = null;

	constructor(
		private readonly _apiToken: string,
		private readonly _onChanged: OnTrackingChanged,
	)
	{
	}

	public startTracking(timeout: number): void
	{
		this.checkTracking().finally(() => {
			setTimeout(() => {
				this.startTracking(timeout);
			}, timeout);
		});
	}

	public async getRunningEntry(): Promise<TimeEntry | null>
	{
		return await fetch('https://api.track.toggl.com/api/v9/me/time_entries/current', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Basic ${this._apiToken}`,
			},
		})
			.then((res) => {
				return res.json();
			})
			.catch(err => {
				console.error(err);
				return null;
			});
	}

	public async stopRunningNow(): Promise<void>
	{
		if (this._currentEntry === null) {
			return;
		}

		await fetch(`https://api.track.toggl.com/api/v9/workspaces/${this._currentEntry.workspace_id}/time_entries/${this._currentEntry.id}/stop`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Basic ${this._apiToken}`,
			},
		}).catch(err => {
			console.error(err);
		});
	}

	public async stopRunning(stopAt: Date): Promise<void>
	{
		if (this._currentEntry === null) {
			return;
		}

		await fetch(`https://api.track.toggl.com/api/v9/workspaces/${this._currentEntry.workspace_id}/time_entries/${this._currentEntry.id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Basic ${this._apiToken}`,
			},
			body: JSON.stringify({
				stop: stopAt.toISOString(),
			}),
		}).catch(err => {
			console.error(err);
		});
	}

	private async checkTracking(): Promise<void>
	{
		const entry = await this.getRunningEntry();
		if (entry?.id === this._currentEntry?.id) {
			return;
		}

		this._currentEntry = entry;
		this._onChanged(this._currentEntry !== null);
	}
}
