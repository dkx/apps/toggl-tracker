import * as fs from 'node:fs/promises';
import { Validator, ValidatorResult } from 'jsonschema';
import { Config01, StartNotification01 } from '../schema/config-01';
import Config01JsonSchema from '../schema/config-01.json';


export function createTogglAuth(configuration: Config01): string
{
	return Buffer.from(`${configuration.toggl.apiKey}:api_token`).toString('base64');
}

export async function createConfigurationFromFile(): Promise<Config01 | false>
{
	const filePath = getConfigurationPath();
	if (filePath === false) {
		console.error('No configuration file found');
		return false;
	}

	const file = await fs.readFile(filePath, 'utf-8');
	let json = JSON.parse(file) as Config01;

	const validation = await validateConfigurationFile(json);
	if (!validation.valid) {
		console.error('Invalid configuration file', validation.errors);
		return false;
	}

	if (json.startNotifications !== undefined) {
		json = {
			...json,
			startNotifications: json.startNotifications.map(n => ({
				windowTitleContains: n.windowTitleContains.toLowerCase(),
			})),
		};
	}

	return json;
}

function getConfigurationPath(): string | false
{
	const filePath = process.env.TOGGL_TRACKER_CONFIGURATION;
	if (typeof filePath === 'string') {
		return filePath;
	}

	return false;
}

async function validateConfigurationFile(json: unknown): Promise<ValidatorResult>
{
	const validator = new Validator();
	return validator.validate(json, Config01JsonSchema);
}
