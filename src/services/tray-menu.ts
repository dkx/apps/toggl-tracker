import { Tray, Menu, MenuItem } from 'electron';
import { assetPath } from '../utils';


const ICON_WHITE = assetPath('icon-white.png');
const ICON_PINK = assetPath('icon-pink.png');

export type OnOpenRequested = () => void;
export type OnQuitRequested = () => void;
export type OnToggleRequested = () => void;

export class TrayMenu
{
	private readonly _tray: Tray;

	private readonly _menu: Menu;

	private readonly _menuItemStart: MenuItem;

	private readonly _menuItemStop: MenuItem;

	constructor(onOpenRequested: OnOpenRequested, onQuitRequested: OnQuitRequested, onToggleRequested: OnToggleRequested)
	{
		this._tray = new Tray(ICON_WHITE);

		this._menu = Menu.buildFromTemplate([
			{ label: 'Open', type: 'normal', click: onOpenRequested },
			{ label: 'Start', type: 'normal', click: onToggleRequested, id: 'start' },
			{ label: 'Stop', type: 'normal', click: onToggleRequested, id: 'stop', enabled: false },
			{ type: 'separator' },
			{ label: 'Quit', type: 'normal', click: onQuitRequested },
		]);

		this._menuItemStart = this._menu.getMenuItemById('start');
		this._menuItemStop = this._menu.getMenuItemById('stop');

		this._tray.setContextMenu(this._menu);
	}

	public setTrackingState(tracking: boolean): void
	{
		this._tray.setImage(tracking ? ICON_PINK : ICON_WHITE);

		if (tracking) {
			this._menuItemStart.enabled = false;
			this._menuItemStop.enabled = true;
		} else {
			this._menuItemStart.enabled = true;
			this._menuItemStop.enabled = false;
		}

		// Without this, the menu enabled states will not update
		this._tray.setContextMenu(this._menu);
	}
}
