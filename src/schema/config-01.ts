export declare interface Config01
{
	readonly toggl: {
		readonly apiKey: string,
		readonly autoRefresh?: number,
	},
	readonly startNotifications?: readonly StartNotification01[],
	readonly stopNotification?: StopNotification01,
}

export declare interface StartNotification01
{
	readonly windowTitleContains: string,
}

export declare interface StopNotification01
{
	readonly idleAfter: number,
}
