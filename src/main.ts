import { app } from 'electron';
import { Application } from './application';
import { createConfigurationFromFile } from './services/configuration';


app.on('ready', async () => {
	const configuration = await createConfigurationFromFile();
	if (configuration === false) {
		app.quit();
		return;
	}

	const application = new Application(configuration);
	await application.start();
});

app.on('window-all-closed', () => {
	app.quit();
});
