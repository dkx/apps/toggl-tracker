import { app } from 'electron';
import path from 'node:path';


export function assetPath(name: string): string
{
	return path.join(app.getAppPath(), 'src/assets/', name);
}
