import { app, shell, Notification, dialog } from 'electron';
import { WindowMonitor } from './services/window-monitor';
import { Client } from './services/client';
import { Config01 } from './schema/config-01';
import { createTogglAuth } from './services/configuration';
import { TrayMenu } from './services/tray-menu';
import { IdleMonitor } from './services/idle-monitor';


const TOGGL_TRACKING_URL = 'https://track.toggl.com/timer';
const IDLE_REFRESH_TIMEOUT = 1000;

export class Application
{
	private readonly _windowMonitor: WindowMonitor | null = null;

	private readonly _idleMonitor: IdleMonitor | null = null;

	private readonly _client: Client;

	private readonly _tray: TrayMenu;

	private _trackerRunning = false;

	private _canShowStartNotification = true;

	private _canShowStopDialog = true;

	constructor(
		private readonly _configuration: Config01,
	)
	{
		if (this._configuration.startNotifications?.length > 0) {
			this._windowMonitor = new WindowMonitor(this.handleWindowTitleChanged.bind(this));
		}

		if (this._configuration.stopNotification !== undefined) {
			this._idleMonitor = new IdleMonitor(this.handleIdleTimeChanged.bind(this));
		}

		this._client = new Client(createTogglAuth(this._configuration), this.handleTogglTrackingChanged.bind(this));
		this._tray = new TrayMenu(
			this.handleTrayOpenRequested.bind(this),
			this.handleTrayQuitRequested.bind(this),
			this.handleTrayToggleRequested.bind(this),
		);
	}

	public async start(): Promise<void>
	{
		console.log('[RESOURCES PATH]', process.resourcesPath);
		console.log('[APP PATH]', app.getAppPath());

		if (this._configuration.toggl.autoRefresh !== undefined) {
			this._client.startTracking(this._configuration.toggl.autoRefresh);
		}

		if (this._windowMonitor !== null) {
			await this._windowMonitor.startMonitor();
		}
	}

	private handleWindowTitleChanged(title: string): void
	{
		if (this._trackerRunning || !this._canShowStartNotification) {
			return;
		}

		console.log('[WINDOW TITLE]', title);

		title = title.toLowerCase();

		for (const item of this._configuration.startNotifications ?? []) {
			if (title.includes(item.windowTitleContains)) {
				this.showStartNotification();
				break;
			}
		}
	}

	private async handleTogglTrackingChanged(running: boolean): Promise<void>
	{
		console.log('[TOGGL TRACKING]', running);
		this._trackerRunning = running;
		this._tray.setTrackingState(running);

		if (this._windowMonitor !== null) {
			if (running) {
				this._windowMonitor.stopMonitor();
			} else {
				await this._windowMonitor.startMonitor();
			}
		}

		if (this._idleMonitor !== null) {
			if (running) {
				await this._idleMonitor.startMonitor(IDLE_REFRESH_TIMEOUT);
			} else {
				this._idleMonitor.stopMonitor();
			}
		}
	}

	private handleTrayOpenRequested(): void
	{
		this.openTogglTracking();
	}

	private handleTrayQuitRequested(): void
	{
		app.quit();
	}

	private async handleTrayToggleRequested(): Promise<void>
	{
		if (this._trackerRunning) {
			await this._client.stopRunningNow();
		} else {
			this.openTogglTracking();
		}
	}

	private async handleIdleTimeChanged(idle: bigint): Promise<void>
	{
		if (!this._trackerRunning || !this._canShowStopDialog || this._configuration.stopNotification === undefined) {
			return;
		}

		console.log('[IDLE TIME]', idle);

		if (idle >= this._configuration.stopNotification.idleAfter * 1000) {
			await this.showStopDialog();
		}
	}

	private openTogglTracking()
	{
		shell.openExternal(TOGGL_TRACKING_URL);
	}

	private showStartNotification(): void
	{
		if (!this._canShowStartNotification) {
			return;
		}

		this._canShowStartNotification = false;

		setTimeout(() => {
			this._canShowStartNotification = true;
		}, 5000);

		const notification = new Notification({
			title: 'Toggl Tracker',
			body: 'Don\'t forget to track your time!',
		});

		notification.addListener('click', () => this.openTogglTracking());
		notification.show();
	}

	private async showStopDialog(): Promise<void>
	{
		if (!this._canShowStopDialog) {
			return;
		}

		this._canShowStopDialog = false;

		const now = new Date();

		const result = await dialog.showMessageBox({
			title: 'Toggl Tracker',
			message: `You've been idle since ${now.toLocaleTimeString()}. Time to pause the tracking?`,
			type: 'question',
			buttons: ['Stop tracking', 'Keep tracking'],
		});

		this._canShowStopDialog = true;

		if (result.response === 0) {
			await this._client.stopRunning(now);
		}
	}
}
