export default {
	packagerConfig: {
		icon: './src/assets/icon-pink.svg',
	},
	makers: [
		{
			name: '@electron-forge/maker-zip'
		},
	],
};
