# Toggl Tracker for Linux

Supports only Gnome for now.

## Dependencies

https://gitlab.com/dkx/gnome/focused-window-monitor

## Installation

Download from [releases](https://gitlab.com/dkx/apps/toggl-tracker/-/releases)

## Usage

Create configuration file somewhere in your home directory based on [schema](https://gitlab.com/dkx/apps/toggl-tracker/-/raw/main/src/schema/config-01.json):

```json
{
    "$schema": "https://gitlab.com/dkx/apps/toggl-tracker/-/raw/master/src/schema/config-01.json",
    "useExternalBrowser": true,
    "toggl": {
        "apiKey": "--- your API key ---",
        "autoRefresh": 1000
    },
    "startNotifications": [
        {
            "windowTitleContains": "1st project name to monitor"
        },
        {
            "windowTitleContains": "2nd project name to monitor"
        }
    ],
    "stopNotifications": {
        "idleAfter": 300
    }
}
```

Run the app:

```bash
$ export TOGGL_TRACKER_CONFIGURATION=/path/to/your/config.json
$ /path/to/toggl-tracker
```
